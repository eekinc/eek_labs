#!/bin/bash

mkdir -p ./src/components/$1
component=$(basename $1)
cp -f ./scripts/generic_component.html ./src/components/$1/$component.html
cp -f ./scripts/generic_component_virtual.html ./src/components/$1/_$component.html
cp -f ./scripts/generic_component.js ./src/components/$1/$component.js
cp -f ./scripts/generic_component.css ./src/components/$1/$component.css