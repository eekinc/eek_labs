[[__top__/src/js/JSUtils/Component.js]]

/**
 * @class
 */
class MyComponent extends Component {
    /**
     * @constructor
     */
    constructor(dataObjectA, dataObjectB) {
        const params = Object.assign({}, dataObjectA, dataObjectB);
        
        super(MyComponent.ID.TEMPLATE.THIS, params);
    }

    /**
     * @public
     */
    render() {
        return super.render();
    }
}

MyComponent.ID = {
    TEMPLATE: {
        THIS: 'MyComponent_template',
    },
};