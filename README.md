# Mortgage Interest Calculator

## Instructions
In this project you will use HTML, CSS, and JavaScript to create a simple mortgage interest calculator app.

Make sure to follow the Eek style guidelines: https://gitlab.com/eekinc/style_guides

And the Eek code review guidelines: https://gitlab.com/eekinc/code-review

You will make use of the following:
* [Slurp](https://github.com/jdsutton/Slurp) - For building you project in real time
* [JSUtils](https://github.com/jdsutton/JSUtils) - For manipulating the DOM.
* [JsRender](https://github.com/jdsutton/JsRender) - For rendering templates.

### Part 0: Setup
Clone and install this project:

```
git clone --recursive https://gitlab.com/eekinc/eek_labs.git
cd eek_labs
make init_submodules
make
```

### Part 1: Calculator
We want to calculate how much interest will accrue on a mortgage loan over a number of years.

1. Create a webpage with inputs for the loan principle, interest rate percentage, and number of years. Make sure to include clear labels for each.
2. Create a button labelled "Calculate", which we will use to calculate the interest.
3. Implement a function which reads the inputs from the page, computes interest ([continuous](http://www.meta-financial.com/lessons/compound-interest/continuously-compounded-interest.php)), and prints the result to the console. 
4. Create a merge request with your changes in a new repository and assign it to your supervisor to review.

### Part 2: Style
1. Give your web app a fancy style makeover using CSS and Flexbox.
2. Create a merge request with your changes in a new branch and assign it to your supervisor to review.

## Part 3: Documentation
1. Using [jsdoc](http://usejsdoc.org) style, document your JavaScript's code classes and functions appropriately.
2. Use `make docs` to build your documentation. Open the index.html under `documentation/` to see that it looks as you would expect.
2. Create a merge request with your changes in a new branch and assign it to your supervisor to review.

## Part 4: Rendering
1. Use JsRender to show the result of your calculation in the UI somewhere.
2. Create a merge request with your changes in a new branch and assign it to your supervisor to review.

### Part 5: Calculation History, Components
1. Now we will create a list of past calculations. It is up to you to decide how to display each entry in the history.
2. Using HTML, create a scrollable container which you will render history items into
3. Create a new component to represent the history items: `./scripts/createNewComponent CalculationHistoryItem`
4. Edit your component's HTML and CSS as you please
  1. Remember you can use the `[[myParamName]]` syntax to insert values into the HTML template. See JsRender.
5. After a calculation is performed:
  1.  Create an instance of your component class, render it, and add it to your container.
  2. Review the documentation for `Component` and `Dom`.
2. Create a merge request with your changes in a new branch and assign it to your supervisor to review.

### Part 6: Sandbox
1. This part is optional.
2. Add any cool features to your app that you like!